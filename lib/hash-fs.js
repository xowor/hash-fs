var async = require('async');
var path = require('path');
var fs = require('fs');

var HashFS = function(config) {
  /**
   * Generates an array of ordered prefixes, based on the 'obj' Object passed. If defined,
   * the 'prefix' function of configuration is used. Otherwise, `obj` is expected to have
   * an `id` string property, and the prefix will contain two strings, containing the
   * first 4 characters of the id string.
   */
  var _prefix = function(obj) {
    if (obj === null) {
      throw new Error('Object is null');
    } else if (typeof obj === 'undefined') {
      throw new Error('Object is undefined');
    } else {
      if (config.prefix) {
        return config.prefix(obj);
      } else {
        if (!obj.id) {
          throw new Error('Object doesn\'t have an \'id\' property');
        } else {
          return [
            obj.id.substring(0, 2),
            obj.id.substring(2, 4),
          ];
        }
      }
    }
  };

  /**
   * Generates a name string, that will be used for the file name. If defined,
   * the 'name' function of configuration is used. Otherwise, `obj` is expected to have
   * a `name` string property.
   */
  var _name = config.name || function(obj) {
    if (!obj.id && !obj.name) {
      throw new Error('Object doesn\'t have an \'id\' or a \'name\' property');
    } else if (obj.name) {
      return obj.name;
    } else {
      return obj.id;
    }
  };

  var _ext = config.ext || 'json';

  var _serialize = config.serialize || JSON.stringify;

  var _deserialize = config.deserialize || JSON.parse;

  var _root = config.root || __dirname;

  this.get = function(obj, callback) {
    try {
      var prefix = _prefix(obj);
      var name = _name(obj);
      if (!prefix) {
        callback(new Error('Prefix is ' + typeof prefix));
      } else if (!name) {
        callback(new Error('Name is ' + typeof name));
      } else {
        fs.readFile(_root + path.sep + prefix.join(path.sep) + path.sep + name + '.' + _ext, 'utf-8', function(err, data) {
          if (err) {
            throw callback(err);
          } else {
            callback(err, _deserialize(data));
          }
        });
      }
    } catch (e) {
      callback(e);
    }
  };

  this.getPath = function(obj, callback) {
    try {
      var prefix = _prefix(obj);
      var name = _name(obj);
      if (!prefix) {
        callback(new Error('Prefix is ' + typeof prefix));
      } else if (!name) {
        callback(new Error('Name is ' + typeof name));
      } else {
        var filePath = _root + path.sep + prefix.join(path.sep) + path.sep + name + '.' + _ext;
        fs.lstat(filePath, function(err, stats) {
          if (err) {
            callback(err);
          } else {
            callback(null, filePath);
          }
        });
      }
    } catch (e) {
      callback(e);
    }
  };

  this._createDirectories = function(prefix, callback) {
    var createDirsFns = [];
    prefix.forEach(function(p, i) {
      createDirsFns.push(function(callback) {
        var dir = _root + path.sep + prefix.slice(0, i + 1).join(path.sep);

        try {
          dirStats = fs.lstatSync(dir);
          if (!dirStats.isDirectory()) {
            fs.mkdir(dir, function(err) {
              callback(err);
            });
          } else {
            callback(null);
          }
        }
        catch (e) {
          fs.mkdir(dir, function(err) {
            callback(err);
          });
        }
      });
    });

    async.series(createDirsFns, function(err, results) {
      callback(err);
    });
  };

  this._deleteDirectories = function(prefix, callback) {
    var delDirsFns = [];
    prefix.forEach(function(p, i) {
      delDirsFns.push(function(callback) {
        var dir = _root + path.sep + prefix.slice(0, i + 1).join(path.sep);

        fs.rmdir(dir, function(err) {
          if (err) {
            if (err.code === 'ENOTEMPTY') {
              callback();
            } else {
              callback(err);
            }
          } else {
            callback();
          }
        });
      });
    });

    delDirsFns.reverse();

    async.series(delDirsFns, function(err, results) {
      callback(err);
    });
  };

  this.put = function(obj, callback) {
    try {
      var prefix = _prefix(obj);
      if (!prefix || prefix.constructor != Array) {
        callback(new Error('Prefix is not valid'));
      } else {
        this._createDirectories(prefix, function(err, results) {
          if (err) {
            callback(err);
          } else {
            var name = _name(obj);
            if (name) {
              fs.writeFile(_root + path.sep + prefix.join(path.sep) + path.sep + name + '.' + _ext, _serialize(obj), function(err) {
                callback(err);
              });
            } else {
              callback(new Error());
            }
          }
        });
      }
    } catch (e) {
      callback(e);
    }
  };

  this.putPath = function(obj, sourcePath, callback) {
    try {
      var prefix = _prefix(obj);
      if (!prefix || prefix.constructor != Array) {
        callback(new Error('Prefix is not valid'));
      } else {
        this._createDirectories(prefix, function(err, results) {
          if (err) {
            callback(err);
          } else {
            var name = _name(obj);
            if (name) {
              fs.rename(sourcePath, _root + path.sep + prefix.join(path.sep) + path.sep + name + '.' + _ext, function(err) {
                fs.writeFile(_root + path.sep + prefix.join(path.sep) + path.sep + name + '.' + _ext, _serialize(obj), function(err) {
                  callback(err);
                });
              });
            } else {
              callback(new Error());
            }
          }
        });
      }
    } catch (e) {
      callback(e);
    }
  };

  this.each = function() {
    var fn = arguments[0];
    var filter = null;
    if (arguments.length > 2) {
      filter = arguments[1];
    }

    var callback = arguments[arguments.length - 1];
    /* Reads thi given directory, returning an array of directories and executing the each function on files */
    var getFilesDirectories = function(srcpath, prefix, callback) {
      prefix = prefix.concat(srcpath.substring(srcpath.lastIndexOf('/') + 1, srcpath.length));
      return fs.readdir(srcpath, function(err, files) {
        if (err) throw err;
        var directories = [];
        async.series(files.map(function(file) {
          return function(callback) {
            fs.stat(path.join(srcpath, file), function(err, stats) {
              if (err) {
                throw err;
              } else if (stats.isDirectory()) {
                directories.push(path.join(srcpath, file));
                callback();
              } else if (stats.isFile()) {
                if (!filter || (filter && typeof filter === 'function' && filter(file, prefix) === true)) {
                  fn(file, prefix, callback);
                } else {
                  callback();
                }
              } else {
                callback();
              }
            });
          };
        }),

        function(err, results) {
          if (err) {
            throw err;
          } else {
            callback(directories, prefix);
          }
        });
      });
    };

    var iterate = function(dir, prefix, callback) {
      getFilesDirectories(dir, prefix, function(directories, prefix) {
        // files.files.forEach(function(file) {
        //   filesList.push(file);
        // });
        async.series(directories.map(function(directory) {
          return function(callback) {
            // console.log('iterate');
            iterate(directory, prefix, callback);
          };
        }),

        function(err, results) {
          if (err) throw err;
          else callback();
        });
      });
    };

    iterate(_root, [], callback);
  };

  this.del = function(obj, callback) {
    var _this = this;
    try {
      var prefix = _prefix(obj);
      var name = _name(obj);
      if (!prefix || prefix.constructor != Array) {
        callback(new Error('Prefix is not valid'));
      } else {
        fs.unlink(_root + path.sep + prefix.join(path.sep) + path.sep + name + '.' + _ext, function(err) {
          if (err) callback(err);
          _this._deleteDirectories(prefix, function(err, results) {
            callback(err);
          });
        });
      }
    } catch (e) {
      callback(e);
    }
  };

  this.prefix = _prefix;
};

var storageInstance;
module.exports = function(config) {
  if (config) {
    storageInstance = new HashFS(config);
  }

  return storageInstance;
};
