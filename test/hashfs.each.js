var expect = require('chai').expect;
var fs = require('fs');
var HashFS = require('../');

describe('each()', function() {
  var storage;
  before(function(done) {
    storage = new HashFS({
      root: __dirname + '/testroot',
      prefix: function(obj) {
        return [
          obj.foo,
          obj.bar,
        ];
      },
    });
    fs.mkdir(__dirname + '/testroot/foo', function(err) {
      if (err) throw err;
      fs.mkdir(__dirname + '/testroot/foo/bar', function(err) {
        if (err) throw err;
        fs.mkdir(__dirname + '/testroot/goo', function(err) {
          if (err) throw err;
          fs.mkdir(__dirname + '/testroot/goo/car', function(err) {
            if (err) throw err;
            fs.writeFile(__dirname + '/testroot/foo/bar/fooBarTestFile', { title: 'Foo Bar' }, function(err) {
              if (err) throw err;
              fs.writeFile(__dirname + '/testroot/goo/car/gooCarTestFile', { title: 'Coo Bar' }, function(err) {
                if (err) throw err;
                fs.writeFile(__dirname + '/testroot/goo/gooTestFile', { title: 'Goo' }, function(err) {
                  if (err) throw err;
                  done();
                });
              });
            });
          });
        });
      });
    });
  });

  after(function(done) {
    fs.unlink(__dirname + '/testroot/foo/bar/fooBarTestFile', function(err) {
      if (err) throw err;
      fs.unlink(__dirname + '/testroot/goo/car/gooCarTestFile', function(err) {
        if (err) throw err;
        fs.unlink(__dirname + '/testroot/goo/gooTestFile', function(err) {
          if (err) throw err;
          fs.rmdir(__dirname + '/testroot/goo/car', function(err) {
            if (err) throw err;
            fs.rmdir(__dirname + '/testroot/goo', function(err) {
              if (err) throw err;
              fs.rmdir(__dirname + '/testroot/foo/bar', function(err) {
                if (err) throw err;
                fs.rmdir(__dirname + '/testroot/foo', function(err) {
                  if (err) throw err;
                  done();
                });
              });
            });
          });
        });
      });
    });
  });

  it('iterate over all files', function(done) {
    var files = [];
    storage.each(function(filename, prefix, callback) {
      files.push({
        filename: filename,
        prefix: prefix,
      });
      callback();
    },

    function() {
      expect(files).to.have.length(3);
      expect(files[0].filename).to.be.a('string').that.equals('fooBarTestFile');
      expect(files[0].prefix).to.be.an('array').that.eql(['testroot', 'foo', 'bar']);
      expect(files[1].filename).to.be.a('string').that.equals('gooTestFile');
      expect(files[1].prefix).to.be.an('array').that.eql(['testroot', 'goo']);
      expect(files[2].filename).to.be.a('string').that.equals('gooCarTestFile');
      expect(files[2].prefix).to.be.an('array').that.eql(['testroot', 'goo', 'car']);
      done();
    });
  });

  it('iterate over files whose names  matches a regexp', function(done) {
    var files = [];
    storage.each(function(filename, prefix, callback) {
      files.push({
        filename: filename,
        prefix: prefix,
      });
      callback();
    },

    function(filename, prefix) {
      return (/goo./g).test(filename);
    },

    function() {
      expect(files).to.have.length(2);
      expect(files[0].filename).to.be.a('string').that.equals('gooTestFile');
      expect(files[0].prefix).to.be.an('array').that.eql(['testroot', 'goo']);
      expect(files[1].filename).to.be.a('string').that.equals('gooCarTestFile');
      expect(files[1].prefix).to.be.an('array').that.eql(['testroot', 'goo', 'car']);
      done();
    });
  });

  it('iterate over files whose prefix contains a directory', function(done) {
    var files = [];
    storage.each(function(filename, prefix, callback) {
      files.push({
        filename: filename,
        prefix: prefix,
      });
      callback();
    },

    function(filename, prefix) {
      return (prefix.indexOf('goo') > 0);
    },

    function() {
      expect(files).to.have.length(2);
      expect(files[0].filename).to.be.a('string').that.equals('gooTestFile');
      expect(files[0].prefix).to.be.an('array').that.eql(['testroot', 'goo']);
      expect(files[1].filename).to.be.a('string').that.equals('gooCarTestFile');
      expect(files[1].prefix).to.be.an('array').that.eql(['testroot', 'goo', 'car']);
      done();
    });
  });

});
