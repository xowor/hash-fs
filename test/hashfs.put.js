var expect  = require('chai').expect;
var fs = require('fs');
var HashFS = require('../');

describe('put()', function() {
  var storage;
  before(function(done) {
    storage = new HashFS({
      root: __dirname + '/testroot',
      prefix: function(obj) {
        return [
          obj.foo,
          obj.bar,
        ];
      },
    });
    done();
  });

  it('put a file', function(done) {
    storage.put({ name: 'testfile', foo: 'foo', bar: 'bar', testFile: true }, function(err) {
      if (err) throw err;
      expect(err).to.be.null;
      fs.readFile(__dirname + '/testroot/foo/bar/testfile.json', 'utf-8', function(err, file) {
        if (err) throw err;
        expect(err).to.be.null;
        expect(file).to.be.a('string');
        expect(file).to.equal('{"name":"testfile","foo":"foo","bar":"bar","testFile":true}');
        fs.unlink(__dirname + '/testroot/foo/bar/testfile.json', function(err) {
          if (err) throw err;
          fs.rmdir(__dirname + '/testroot/foo/bar', function(err) {
            if (err) throw err;
            fs.rmdir(__dirname + '/testroot/foo/', function(err) {
              if (err) throw err;
              done();
            });
          });
        });
      });
    });
  });

  it('return an error in case of null object', function(done) {
    storage.put(null, function(err) {
      expect(err).to.be.not.null;
      expect(err).to.have.property('message')
        .that.equals('Object is null');
      done();
    });
  });
});
