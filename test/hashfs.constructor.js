var expect = require('chai').expect;
var fs = require('fs');
var HashFS = require('../');

describe('Constructor', function() {
  it('create a new storage', function(done) {
    storage = new HashFS({
      root: __dirname + '/testroot',
      prefix: function(obj) {
        return [
          obj.foo,
          obj.bar,
        ];
      },
    });
    expect(storage).to.be.an('object');
    expect(storage.get).to.be.a('function');
    expect(storage.put).to.be.a('function');
    expect(storage.each).to.be.a('function');
    expect(storage.del).to.be.a('function');
    expect(storage.prefix).to.be.a('function');
    done();
  });
});
