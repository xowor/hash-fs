var expect  = require('chai').expect;
var fs = require('fs');
var HashFS = require('../');

describe('putPath()', function() {
  var storage;
  before(function(done) {
    storage = new HashFS({
      root: __dirname + '/testroot',
      prefix: function(obj) {
        return [
          obj.foo,
          obj.bar,
        ];
      },
    });
    done();
  });

  it('put a file by path', function(done) {
    fs.writeFile(__dirname + '/testroot/testfile.json', '{"name":"testfile","foo":"foo","bar":"bar","testFile":true}', 'utf-8', function(err) {
      if (err) throw err;

      storage.putPath({ name: 'testfile', foo: 'foo', bar: 'bar', testFile: true }, __dirname + '/testroot/testfile.json', function(err) {
        expect(err).to.be.null;
        fs.readFile(__dirname + '/testroot/foo/bar/testfile.json', 'utf-8', function(err, file) {
          expect(err).to.be.null;
          expect(file).to.be.a('string');
          expect(file).to.equal('{"name":"testfile","foo":"foo","bar":"bar","testFile":true}');
          fs.unlink(__dirname + '/testroot/foo/bar/testfile.json', function() {
            fs.rmdir(__dirname + '/testroot/foo/bar', function() {
              fs.rmdir(__dirname + '/testroot/foo/', done);
            });
          });
        });
      });
    });
  });
});
