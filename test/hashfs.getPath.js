var expect  = require('chai').expect;
var fs = require('fs');
var HashFS = require('../');

describe('getPath()', function() {
  var storage;
  before(function(done) {
    storage = new HashFS({
      root: __dirname + '/testroot',
      prefix: function(obj) {
        return [
          obj.foo,
          obj.bar,
        ];
      },
    });
    fs.mkdir(__dirname + '/testroot/foo', function() {
      fs.mkdir(__dirname + '/testroot/foo/bar', function() {
        done();
      });
    });
  });

  after(function(done) {
    fs.rmdir(__dirname + '/testroot/foo/bar', function() {
      fs.rmdir(__dirname + '/testroot/foo', function() {
        done();
      });
    });
  });

  beforeEach(function(done) {
    fs.writeFile(__dirname + '/testroot/foo/bar/testfile.json', JSON.stringify({ testFile: true }), function(err) {
      if (err) throw err;
      done();
    });
  });

  afterEach(function(done) {
    fs.unlink(__dirname + '/testroot/foo/bar/testfile.json', function(err) {
      if (err) throw err;
      done();
    });
  });

  it('get a file', function(done) {
    storage.getPath({ name: 'testfile', foo: 'foo', bar: 'bar' }, function(err, filePath) {
      expect(err).to.be.null;
      expect(filePath).to.be.a('string');
      expect(filePath).to.equal(__dirname + '/testroot/foo/bar/testfile.json');
      done();
    });
  });

  it('return an error in case of non-existing filePath', function(done) {
    storage.getPath({ name: 'wrongfile', foo: 'foo', bar: 'bar' }, function(err, filePath) {
      expect(err).to.be.not.null;
      expect(err).to.have.property('code')
        .that.equals('ENOENT');
      done();
    });
  });

  it('return an error in case of non-existing prefix filePath', function(done) {
    storage.getPath({ name: 'testfile', foo: 'foo', bar: 'wrong' }, function(err, filePath) {
      expect(err).to.be.not.null;
      expect(err).to.have.property('code')
        .that.equals('ENOENT');
      done();
    });
  });

  it('return an error in case of null object', function(done) {
    storage.getPath(null, function(err, filePath) {
      expect(err).to.be.not.null;
      expect(err).to.have.property('message')
        .that.equals('Object is null');
      done();
    });
  });
});
