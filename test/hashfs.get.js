var expect  = require('chai').expect;
var fs = require('fs');
var HashFS = require('../');

describe('get()', function() {
  var storage;
  before(function(done) {
    storage = new HashFS({
      root: __dirname + '/testroot',
      prefix: function(obj) {
        return [
          obj.foo,
          obj.bar,
        ];
      },
    });
    fs.mkdir(__dirname + '/testroot/foo', function() {
      fs.mkdir(__dirname + '/testroot/foo/bar', function() {
        done();
      });
    });
  });

  after(function(done) {
    fs.rmdir(__dirname + '/testroot/foo/bar', function() {
      fs.rmdir(__dirname + '/testroot/foo', function() {
        done();
      });
    });
  });

  beforeEach(function(done) {
    fs.writeFile(__dirname + '/testroot/foo/bar/testfile.json', JSON.stringify({ testFile: true }), function(err) {
      if (err) throw err;
      done();
    });
  });

  afterEach(function(done) {
    fs.unlink(__dirname + '/testroot/foo/bar/testfile.json', function(err) {
      if (err) throw err;
      done();
    });
  });

  it('get a file', function(done) {
    storage.get({ name: 'testfile', foo: 'foo', bar: 'bar' }, function(err, file) {
      expect(err).to.be.null;
      expect(file).to.be.an('object');
      expect(file).to.have.property('testFile')
        .that.is.true;
      done();
    });
  });

  it('return an error in case of non-existing file', function(done) {
    storage.get({ name: 'wrongfile', foo: 'foo', bar: 'bar' }, function(err, file) {
      expect(err).to.be.not.null;
      expect(err).to.have.property('code')
        .that.equals('ENOENT');
      done();
    });
  });

  it('return an error in case of non-existing prefix filePath', function(done) {
    storage.get({ name: 'testfile', foo: 'foo', bar: 'wrong' }, function(err, file) {
      expect(err).to.be.not.null;
      expect(err).to.have.property('code')
        .that.equals('ENOENT');
      done();
    });
  });

  it('return an error in case of null object', function(done) {
    storage.get(null, function(err, file) {
      expect(err).to.be.not.null;
      expect(err).to.have.property('message')
        .that.equals('Object is null');
      done();
    });
  });
});
