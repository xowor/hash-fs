# Introduction
HashFS is a library for storing (and executing CRUD operations) on files on the filesystem, keeping automatically a taxonomy of directories.
The taxonomy structure is fully customizable.

# Getting started
This example snippet stores on the disk the song metadata as json.

```js
var HashFS = require('hash-fs');
var storage = new HashFS({
  root: __dirname + '/myfilesroot',
  prefix: function(obj) {
    // Directory structure like /myfilesroot/Linkin Park/2000/Hybrid Theory/
    return [
      obj.artist,
      obj.year,
      obj.album,
    ];
  },
  name: function(obj) {
    return obj.title;
  },
  ext: 'json',
  serialize: function(obj) {
    return JSON.stringify(obj);
  },
  deserialize: function(obj) {
    return JSON.parse(obj);
  }
});

var song = {
  'artist': 'Linkin Park',
  'title': 'In the End'
  'album': 'Hybrid Theory',
  'year': '2000'
};

storage.put(song, function (err) {
  if (!err) {
    // The file is stored in /myfilesroot/Linkin Park/2000/Hybrid Theory/In the End.json
    storage.get(song, function (err, gotSong) {
      // `gotSong` is identical to `song`
    });
  }
});

storage.del(song);
```
